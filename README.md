# README #

#This is a custom build to use with Imagemagick with the following options:

#--with-modules
#--disable-hdri

#Delegates: bzlib cairo djvu fftw fontconfig freetype jbig jng jpeg lcms lqr ltdl lzma openexr pangocairo png rsvg tiff wmf x xml zlib

#This was built using checkinstall

#Build options

wget https://sourceforge.net/projects/imagemagick/files/im7-src/ImageMagick-7.0.7-15.tar.gz
tar -xvf ImageMagick-7.0.7-15.tar.gz
cd ImageMagick-7.0.7-15/
./configure --with-modules --disable-hdri
make install
sudo checkinstall